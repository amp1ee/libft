float		ft_map(float value, float a0, float a1, float b0, float b1)
{
	float	perc;

	if (a0 == a1 || b0 == b1)
		return (b0);
	perc = value / (a1 - a0);
	if (perc == 0.0)
		return (b0);
	if (perc == 1.0)
		return (b1);
	return ((1 - perc) * b0 + perc * b1);
}
